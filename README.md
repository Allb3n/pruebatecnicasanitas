# Prueba Tecnica Sanitas - Álvaro Benéitez Lenza📘

## Comenzando  🚀
Bastará con ejecutar el comando para lanzar la app
~~~
ionic serve
~~~

Se ha añadido un test e2e utilizando Cypress, para visualizarlo será 
necesario tener la app corriendo ("ionic serve") y utilizar en otra terminal el comando 
~~~
npm run e2e
~~~