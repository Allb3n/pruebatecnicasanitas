import { Component, OnInit} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AlertController, LoadingController} from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage implements OnInit {
  form:any;

  constructor(
    private formBuilder: FormBuilder,
    private alertController: AlertController,
    public loadingController: LoadingController,
  ) {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      reminder:['']
    });
  }
  ngOnInit(){}

  async alertError() {
    const alert = await this.alertController.create({
      header: 'Datos incorrectos',
      message: 'Verifica tu correo electrónico y/o contraseña e intenta nuevamente.',
      buttons: ['Aceptar']
    });
    await alert.present();
  }

  async alertOk() {
    const alert = await this.alertController.create({
      header: 'Login succed',
      message: 'Welcome!',
      buttons: ['OKEY']
    });
    await alert.present();
  }

  async spinnerLoading() {
    const loading = await this.loadingController.create({
      spinner: 'crescent',
      duration: 2000,
      message: 'Espere por favor',
      translucent: true,
    });
    await loading.present();
    await loading.onDidDismiss();
    this.alertOk();
  }

  submit() {
    if (this.form.valid) {
      this.spinnerLoading()
    }
    else{
      this.alertError()
    }
  }
}